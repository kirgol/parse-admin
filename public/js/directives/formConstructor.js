var rowElement = function ($compile) {
    return {
        restrict: 'E',
        scope: {
            type: '=elType',
            name: '=elName',
            model: '=elModel',
            struct: '=elStruct'
        },
        replace: true,
        link: linkFn,
        controller: function ($scope) {
            $scope.patterns = {
                email: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
                text: /^[a-zA-Z0-9,#.-]+$/,
                number: /^[0-9]+$/,
                password: /^[a-zA-Z0-9]{6,15}$/
            }
        }
    };
    function linkFn(scope, el, attrs, controller) {
        el.html(getTemplate(scope.type));
        $compile(el.contents())(scope);
    }

    function getTemplate(fieldType) {
        switch (fieldType) {
            case 'email':
                return '<div class="col-md-11"><input required ng-pattern="patterns.email" type="email" name="{{name}}" class="form-control" ng-model="model"></div>';
                break;
            case 'text':
                return '<div class="col-md-11"><input required ng-pattern="patterns.text" class="form-control" name="{{name}}" type="text" ng-model="model"></div>';
                break;
            case 'select':
                return '<div class="col-md-11"><select class="form-control" name="{{name}}" ng-options="o for o in struct.options" ng-model="model"></div>';
        }
    }
};
rowElement['$inject'] = ['$compile'];
angular.module('parseAdmin').directive('rowElement', rowElement);


