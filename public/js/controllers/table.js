// Main db table controller
var tableCtrl = function ($scope, $stateParams, $modal, parseCom, $state) {

    //Environment and vars
    $scope.env = {
        tableName: $stateParams.name,
        isLoading: false
    }

    // Pagination settings
    $scope.pagination = {
        perPage: 20,
        currPage: 1,
        init: function () {
            this.totalItems = this.perPage * 2;
            return this;
        }
    }.init();

    $scope.paginationOpts = [20, 40, 60];

    //Setting table rows and cols
    $scope.setTable = function (data) {
        $scope.rows = [];
        $scope.ids = [];
        data.forEach(function (el) {
            $scope.rows.push(el.attributes);
            $scope.ids.push(el.id);
        });
        $scope.cols = _.keys($scope.rows[0]);
    }

    // Get initial rows
    $scope.getRows = function (opts) {
        var opts = opts || {
                fade: false
            };

        if (opts.fade === true) {
            $scope.env.isLoading = true;
        }
        parseCom.getModel($stateParams.name, $scope.pagination).then(function (data) {
            $scope.setTable(data);
            $scope.env.isLoading = false;
        });
    }
    $scope.getRows();

    // Delete row
    $scope.removeRow = function (rows, index, rowId) {
        $modal.open({
            templateUrl: 'views/modal-row-del.html',
            scope: $scope,
            controller: 'rowDelCtrl',
            size: 'sm',
            resolve: {
                delConfirm: function () {
                    return function () {
                        rows.splice(index, 1);
                        parseCom.delRow($scope.env.tableName, rowId);
                    }
                }
            }
        });
    }

    // Opening next page

    $scope.openNext = function () {
        parseCom.getModel($stateParams.name, $scope.pagination).then(function (data) {
            if (data.length) {
                $scope.pagination.totalItems = $scope.pagination.perPage * ($scope.pagination.currPage + 1);
            }
            $scope.env.isLoading = true;
            window.setTimeout(function () {
                $scope.setTable(data);
                $scope.env.isLoading = false;
                $scope.$apply();
            }, 250);
        });
    }

    //Filtering

    $scope.reverse = false;
    $scope.predicate = null;
    $scope.sort = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : true;
    }
}

tableCtrl['$inject'] = ['$scope', '$stateParams', '$modal', 'parseCom', '$state'];

app.controller('tableCtrl', tableCtrl);

var rowDelCtrl = function ($scope, $modal, $modalInstance, delConfirm) {
    $scope.ok = function () {
        delConfirm();
        $modalInstance.dismiss('success');
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }
}
rowDelCtrl['$inject'] = ['$scope', '$modal', '$modalInstance', 'delConfirm'];
app.controller('rowDelCtrl', rowDelCtrl);