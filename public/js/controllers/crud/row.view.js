function viewRowCtrl ($scope,parseData,$stateParams) {
    $scope.model = $stateParams.name,
        $scope.id = $stateParams.rowId,
    $scope.row = parseData[0].attributes;
}
viewRowCtrl['$inject'] = ['$scope','parseData','$stateParams'];

angular.module('parseAdmin').controller('viewRowCtrl',viewRowCtrl);