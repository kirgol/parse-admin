var createRowCtrl = function ($scope, $stateParams, parseCom, parseConfig, $window) {

    $scope.model = $stateParams.name;
    $scope.isSuccess = false;
    $scope.newrow = {};
    console.log(window)
    $scope.saveRow = function () {
        parseCom.updateRow($scope.model, false, $scope.newrow).then(function (data) {
            $scope.isSuccess = true;
            window.setTimeout(function () {
                $scope.isSuccess = false;
                $scope.$apply();
            }, 1500);
        });
    }
    $scope.fields = parseConfig.models[$scope.model].fields;
}

createRowCtrl['$inject'] = ['$scope', '$stateParams', 'parseCom', 'parseConfig', '$window'];
angular.module('parseAdmin').controller('createRowCtrl', createRowCtrl);



