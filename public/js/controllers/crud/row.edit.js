var editRowCtrl = function ($scope, $state, $stateParams, parseCom, parseData,parseConfig) {
    if (parseData.length) { $scope.row = parseData[0].attributes; }
    $scope.model = $stateParams.name;
    $scope.id = $stateParams.rowId;
    $scope.fields = parseConfig.models[$scope.model].fields;
    $scope.updateRow = function () {
        parseCom.updateRow($scope.model, $scope.id, $scope.row).then(function (data) {
            $state.go('model', {name: $scope.model});
        });
    }
}
editRowCtrl['$inject'] = ['$scope', '$state', '$stateParams', 'parseCom', 'parseData','parseConfig'];
angular.module('parseAdmin').controller('editRowCtrl', editRowCtrl);