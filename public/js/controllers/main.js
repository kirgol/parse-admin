var mainCtrl =  function ($scope,parseCom,parseConfig) {
    $scope.models = [];
    _.forOwn(parseConfig.models,function(el,key){
       if (el.visible === true) {
           $scope.models.push(key);
       }
   });
};
mainCtrl['$inject'] = ['$scope','parseCom','parseConfig'];
angular.module('parseAdmin').controller('mainCtrl',mainCtrl);