angular.module('parseAdmin').factory('parseCom', ['$q', function ($q) {

    function errHandler(err) {
        alert('Error:' + err.code + ' ' + err.message);
    }
    return {
        getModel: function (model, opts, cb) {
            var deferred = $q.defer();
            var query = new Parse.Query(model);

            //Pagination handling
            if (opts.perPage) {
                query.limit(opts.perPage);
                if (opts.currPage === 1) {
                    --opts.currPage;
                }
                query.skip(opts.perPage * opts.currPage);
            }

            //Main query
            query.find()
                .then(function (data) {
                    deferred.resolve(data);
                }, function (err) {
                    errHandler(err);
                });
            return deferred.promise;
        },
        getRow: function (model, rowId) {
            var deferred = $q.defer();
            var query = new Parse.Query(model).equalTo('objectId', rowId);
            query.find().then(function (row) {
                deferred.resolve(row);
            }, function (err) {
                errHandler(err);
            });
            return deferred.promise;
        },
        updateRow:

        /* model[String] - Name of the model
         rowId[String|false] - set argument to false if you create a new row
         rowData[Object|Array] - row data to be saved in the database
         */

            function (model, rowId, rowData) {
                var deferred = $q.defer();
                var Row = Parse.Object.extend(model);
                var row = new Row();
                if (rowId) {
                    row.id = rowId;
                }
                _.forOwn(rowData, function (value, key, obj) {
                    row.set(key, obj[key]);
                });
                row.save().then(function (data) {
                    deferred.resolve(data);
                }, function (row, err) {
                    errHandler(err);
                });
                return deferred.promise;
            },
        delRow: function (model, rowId) {
            var deferred = $q.defer();
            var Row = Parse.Object.extend(model);
            var row = new Row();
            row.id = rowId;
            row.destroy().then(function (data) {
                deferred.resolve(data);
            }, function (err) {
                errHandler(err);
            });
        }
    }
}]);

