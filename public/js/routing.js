angular.module('parseAdmin').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('model', {
            url: '/model/:name',
            templateUrl: '/views/table.html',
            controller: 'tableCtrl'
        })
        .state('rowEdit', {
            url: '/model/:name/:rowId/edit',
            templateUrl: '/views/edit-row.html',
            controller: 'editRowCtrl',
            resolve: {
                parseData: parseData
            }
        })
        .state('rowView', {
            url: '/model/:name/:rowId/view',
            templateUrl: '/views/view-row.html',
            controller: 'viewRowCtrl',
            resolve: {
                parseData: parseData
            }
        })
        .state('createRow', {
            url: '/model/:name/create-row',
            templateUrl: '/views/create-row.html',
            controller: 'createRowCtrl'
        });

    $locationProvider.html5Mode(true);

    function parseData(parseCom, $stateParams) {
        return parseCom.getRow($stateParams.name, $stateParams.rowId);
    }
}]);
