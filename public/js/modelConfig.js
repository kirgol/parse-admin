angular.module('parseAdmin').service('parseConfig',function() {
    return {
        models: {
            UserCard:
            {
                name: "UserCard",
                visible: true,
                fields: [
                    {
                        name: "company",
                        title: "Company",
                        type: 'text',
                        validation: "String",
                        required: true
                    },
                    {
                        name: "email",
                        title: "Email",
                        type: 'email',
                        validation: "email",
                        required: true
                    },
                    {
                        name: "gender",
                        title: "Gender",
                        type: "select",
                        options:['male','female'],
                        order: 2,
                        required: true
                    },
                    {
                        name: "location",
                        title: "Location",
                        type: 'text',
                        validation: "String",
                        required: true
                    },
                    {
                        name: "name",
                        title: "Name",
                        type: 'text',
                        validation: "String",
                        required: true
                    },
                    {
                        name: "surname",
                        title: "Surname",
                        type: 'text',
                        validation: "String",
                        required: true
                    },

                ]
            }
    }
}});
