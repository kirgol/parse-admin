var path = require('path'),
    express = require('express'),
    app = express(),
    http = require('http').Server(app),
    config = require('./config.json');

// Setup server

app.use(express.static(path.join(__dirname,'/public/')));

app.all('*/',function(req,res) {
    res.sendFile(path.join(__dirname,'/public/index.html'));
});

http.listen(config.port,config.port, function (err) {
    console.log('Server is listening on port' + ' ' + config.port);
});




