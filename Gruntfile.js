module.exports = function (grunt) {

    grunt.initConfig({
        wiredep: {
            include: {
                src: [
                    "./public/index.html"
                ]
            }
        },
        less: {
            development: {
                options: {
                    paths: ["public/css"]
                },
                files: {
                    "public/css/app.css": "src/myless/app.less"
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['wiredep']);
};
